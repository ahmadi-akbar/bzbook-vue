import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import book from './book';
import auth from './auth';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
  },
  mutations: {
    setLoading: (state, payload) => (state.loading = payload),
  },
  actions: {},
  modules: { book, auth },
  plugins: [createPersistedState()],
});
