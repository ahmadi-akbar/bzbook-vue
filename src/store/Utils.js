import store from './index';

export const Alert = data => store.commit('setAlert', data, { root: true });

export const Loading = data => store.commit('setLoading', data, { root: true });
