import { Loading } from './Utils';
import { AUTH } from './const';
import router from '@/router';

export default {
  namespaced: true,
  state: {
    isAuth: false,
  },
  mutations: {
    setUser: (state, payload) => Object.assign(state, payload),
    setIsAuth: (state, payload) => (state.isAuth = payload),
  },
  actions: {
    register: async ({ commit }, { email, password }) => {
      Loading(true);

      await AUTH()
        .createUserWithEmailAndPassword(email, password)
        .then(user => {
          commit('setUser', user);
          commit('setIsAuth', true);
          router.push('/about');
        })
        .catch(() => {
          commit('setUser', null);
          commit('setIsAuth', false);
          router.push('/');
        });

      Loading(false);
    },
    login: async ({ commit }, { email, password }) => {
      Loading(true);

      await AUTH()
        .signInWithEmailAndPassword(email, password)
        .then(user => {
          commit('setUser', user);
          commit('setIsAuth', true);
          router.push('/about');
        })
        .catch(() => {
          commit('setUser', null);
          commit('setIsAuth', false);
          router.push('/');
        });

      Loading(false);
    },
    logout: ({ commit }) => {
      commit('setUser', null);
      commit('setIsAuth', false);
      router.push('/');

      // AUTH()
      //   .signOut()
      //   .then(() => {
      //     commit('setUser', null);
      //     commit('setIsAuth', false);
      //     router.push('/');
      //   })
      //   .catch(() => {
      //     commit('setUser', null);
      //     commit('setIsAuth', false);
      //     router.push('/');
      //   });
    },
  },
  modules: {},
};
