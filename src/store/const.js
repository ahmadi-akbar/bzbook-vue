import Axios from 'axios';
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const FirebaseConfig = {
  apiKey: 'AIzaSyDVpVfDBxMzblIVUxaGtMjc1lb_knD5ynI',
  authDomain: 'bzbook-99b27.firebaseapp.com',
  databaseURL: 'https://bzbook-99b27.firebaseio.com',
  projectId: 'bzbook-99b27',
  storageBucket: 'bzbook-99b27.appspot.com',
  messagingSenderId: '193594892979',
  appId: '1:193594892979:web:95e595161c114abd4846c6',
  measurementId: 'G-2WPJYHRG4G',
};
export const FirebaseInit = () => firebase.initializeApp(FirebaseConfig);
export const DB = firebase.database;
export const AUTH = firebase.auth;

export const BASE_URL = 'https://api.myjson.com/bins/16q7ww';

export const axios = Axios.create({
  baseURL: BASE_URL,
});
