import { DB } from './const';
import { Loading } from './Utils';
import store from './index';

export default {
  namespaced: true,
  state: {
    books: [],
  },
  mutations: {
    setBooks: (state, payload) => (state.books = payload),
  },
  actions: {
    get: async ({ commit }) => {
      Loading(true);
      await DB()
        .ref('books')
        .once('value')
        .then(snapshot => {
          let val = snapshot.val() || [];
          commit('setBooks', val);
        })
        .catch(err => {
          console.log('err', err);
        });

      Loading(false);
    },
    add: async () => {
      Loading(true);
      const { user = null } = store.state.auth;
      if (!user) return;
      DB()
        .ref('books')
        .push({
          uid: user.uid,
          cover: 'https://preview.ibb.co/bC5ELQ/alex_min.png',
          title: 'Dímelo en palabras',
          des:
            'El polifacético escritor catalán nos muestra en su novela más romántica como, básicamente, nuestra relación es tan fácil como decirnos las cosas como son. Tal cual.',
          price: 100,
          lang: 'es',
        });

      Loading(false);
    },
  },
  modules: {},
};
